SizeByte:               equ 1 
SizeWord:               equ 2 
SizeLong:               equ 4 
SizeTile:               equ 32 
SizePalette:            equ 64 

hblank_counter          equ 0x00FF0000                  ; Start of RAM 
vblank_counter          equ (hblank_counter+SizeLong) 

sprite_attr_x           equ 0x00000001 
sprite_attr_y           equ 0x00000002 
sprite_attr_extended    equ 0x00000004 

LoadPalette: 
; a0 - Palette address 
; d0 - Number of palettes, zero based 
; d1 - Palette index  

        move    d1, d3          ; Move palette index into d3 
        mulu.w  #0x0020, d3 ; Multiply number of palettes by 8 for number of words to read 
        jsr     FormatVDPAddress ; Format d3 to a VDP address 
        or.l    #vdp_write_palettes, d3 ; Splice d3 with the VDP write command 

        move.l  d3, vdp_control ; Set up VDP to write to CRAM address 0x0000 

        mulu.w  #0x0008, d0 ; Multiply number of palettes by 8 for number of words to read 

        @PaletteLoop: 
        move.l  (a0)+, vdp_data         ; Load line into VDP 
        dbra    d0, @PaletteLoop        ; Repeat till d0 is 0 

        rts 

LoadTiles: 
; a0 - Tile address (l) 
; d0 - VRAM address 
; d1 - Num frames 

        swap    d0 
        add.l   #vdp_write_tiles, d0    ; Set write tile mode to the address in VRAM 
        move.l  d0, vdp_control         ; Send the address to the VDP command port 

        subq.b  #0x1, d1                ; Subtract 1 from num frames 
        @FrameCopy:
        move.w  #0x07, d2               ; Set d2 to 8, amount of lines in font 
        @LongCopy: 
        move.l  (a0)+, vdp_data         ; Copy a single line to the VDP data port, auto-increment a0 
        dbra    d2, @LongCopy           ; Continue to `d2` lines is 0 
        dbra    d1, @FrameCopy          ; Continue to `d1` num frames is 0 

        rts 

LoadSpriteTables: 
; a0 - Sprite data address 
; d0 - Number of sprites 

        move.l  #vdp_write_sprite_table, vdp_control 

        subq.b  #0x1, d0 
        @AttrCopy: 
        move.l  (a0)+, vdp_data 
        move.l  (a0)+, vdp_data 
        dbra    d0, @AttrCopy

        rts 

FormatVDPAddress: 
; Formats to VDP address from: 
;       XXXX XXXX XXXX XXXX FEDC BA98 7654 3210 
; to: 
;       XXDC BA98 7654 3210 XXXX XXXX XXXX XXFE 
; d3 - Address to format, also the return value 

        and.l   #0x0000FFFF, d3 ; Clean up value in d1 so we only have a 16 bit value 
        lsl.l   #0x2, d3 
        lsr.w   #0x2, d3 
        swap    d3 

        rts 

WaitVBlankStart: 
        move.w  vdp_control, d0         ; Get VDP status to d0 
        andi.w  #0x0008, d0             ; AND to see if bit 4 is 1, this is the v-blank start bit 
        bne     WaitVBlankStart         ; (branch if not equal) Loop while result in d0 isn't 0 (bit 4 is still flipped) 

        rts 

WaitVBlankEnd: 
        move.w  vdp_control, d0         ; Get VDP status to d0 
        andi.w  #0x0008, d0             ; AND to see if bit 4 is 1, this is the v-blank start bit 
        beq     WaitVBlankEnd           ; (branch if equal) Loop while AND operation result not equal to zero 

        rts 

SetSpritePosX: 
; Set sprite X position 
; d0 (b) - Sprite ID 
; d1 - X coord 

        clr.l   d3                      ; Clear d3 
        
        ; Calc memory location of X coord 
        move.b  d0, d3                  ; Move sprite ID to d3 
        mulu.w  #0x8, d3                ; Sprite array offset by multiply sprite ID by 8 bytes 
        add.b   #0x6, d3                ; Add offset for the word with X coord offset 
        swap    d3 
        ;jsr     FormatVDPAddress        ; Format d3 to a VDP address 
        or.l    #vdp_write_sprite_table, d3 ; Splice d3 with the VDP sprite write command 

        move.l  d3, vdp_control         ; Set dest write address 
        move.w  d1, vdp_data            ; Move X pos to data port 

        rts 
      
SetSpritePosY: 
; Set sprite Y position 
; d0 (b) - Sprite ID 
; d1 - Y coord 

        clr.l   d3                      ; Clear d3 
        
        ; Calc memory location of Y coord 
        move.b  d0, d3                  ; Move sprite ID to d3 
        mulu.w  #0x8, d3                ; Sprite array offset 
        swap    d3 
        ;jsr     FormatVDPAddress        ; Format d3 to a VDP address 
        or.l    #vdp_write_sprite_table, d3 ; Splice d3 with the VDP sprite write command 

        move.l  d3, vdp_control         ; Set dest write address 
        move.w  d1, vdp_data            ; Move Y pos to data port 

        rts 

SetSpriteExtended: 
; Set sprite extended attributes 
; d0 (b) - Sprite ID 
; d1 - Attributes 

        clr.l   d3                      ; Clear d3 
        
        ; Calc memory location of extended attribute 
        move.b  d0, d3                  ; Move sprite ID to d3 
        mulu.w  #0x8, d3                ; Sprite array offset 
        add.b   #0x4, d3                ; Sprite data extended attributes 
        swap    d3 
        ;jsr     FormatVDPAddress        ; Format d3 to a VDP address 
        or.l    #vdp_write_sprite_table, d3 ; Splice d3 with the VDP sprite write command 

        move.l  d3, vdp_control         ; Set dest write address 
        move.w  d1, vdp_data            ; Move X pos to data port 

        rts 

ReadSpriteAttr: 
; d0 (b) - Sprite ID 
; d1 - Attribute 
; d2 - Return result  

        clr.l   d2                      ; Clear d2 to store result 
        clr.l   d3                      ; Clear d3 
        move.b  d0, d3                  ; Store sprite ID in d3 
        mulu.w  #0x8, d3                ; Calc sprite memory loc
        ;add.l   #vdp_sprite_table_addr, d3 ; Add sprite table offset to d3 

        cmpi.w  #sprite_attr_x, d1      ; Test for X pos 
        bne     @noAttrX 
        add.b   #0x6, d3                ; Set d3 to X coord offset 
        swap    d3 
        ;jsr     FormatVDPAddress        ; Format d3 to a VDP address 
        or.l    #vdp_read_sprite_table, d3 ; Splice d3 with the VDP sprite read command 
        move.l  d3, vdp_control         ; Set dest read address in VDP 
        move.w  vdp_data, d2           ; Copy data from VDP to d2 
        @noAttrX: 

        cmpi.w  #sprite_attr_y, d1      ; Test for Y pos 
        bne     @noAttrY 
        swap    d3 
        ;jsr     FormatVDPAddress        ; Format d3 to a VDP address 
        or.l    #vdp_read_sprite_table, d3 ; Splice d3 with the VDP sprite read command 
        move.l  d3, vdp_control         ; Set dest read address in VDP 
        move.w  vdp_data, d2           ; Copy data from VDP to d2 
        @noAttrY: 

        cmpi.w  #sprite_attr_extended, d1      ; Test for extended info 
        bne     @noAttrExt 
        add.b   #0x4, d3                ; Set d3 to flip coord offset 
        swap    d3 
        ;jsr     FormatVDPAddress        ; Format d3 to a VDP address 
        or.l    #vdp_read_sprite_table, d3 ; Splice d3 with the VDP sprite read command 
        move.l  d3, vdp_control         ; Set dest read address in VDP 
        move.w  vdp_data, d2           ; Copy data from VDP to d2 
        @noAttrExt: 

        rts 

AnimateSpriteFwd: 
; Advance sprite to next frame 
; d0 - Sprite address (VRAM) 
; d1 - Size of one sprite frame (in bytes) 
; d2 - Number of anim frames 
; a0 - Address of sprite data (ROM) 
; a1 - Address of animation data (ROM)
; a2 - Address of animation frame counter (RAM, writeable) 

        clr.l   d3  
        move.b  (a2), d3        ; Current frame number to d3 
        addi.b  #0x1, (a2)      ; Advance frame number (addi adds without loading from memory) 
        cmp.b   d3, d2          ; Check new frame number within num anim frames 
        bne     @NotAtEnd       
        move.b  #0x0, (a2)      ; Wrap counter to zero 
        @NotAtEnd: 

        move.b  (a1,d3.w), d4   ; Get original frame index from anim data 
        move.b  (a2), d2        ; Read next frame number from d2 
        move.b  (a1,d3.w), d5   ; Get next frame index (d5) from anim data array 

        cmp.b   d3, d4          ; Has anim frame index changed? 
        beq     @NoChange       ; If not, we're done 

        move.l  a0, d2          ; Sprite data ROM address to d2 
        move.w  d1, d4          ; Move size of one frame to d4 (to preserve d1) 
        mulu.w  d5, d4          ; ROM offset = frame index * frame size (save to d4) 
        add.w   d4, d2          ; Add to sprite data address 
        move.l  d2, a0          ; Put value back in address register 
        move.l  #LizardSpriteOneFrameT, d1  ; HACK 

        jsr LoadTiles           ; Now we load the tiles into VRAM, address in a0 

        @NoChange: 

        rts 