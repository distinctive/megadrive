; Map assets to VRAM 
PixelFontVRAM: 	        equ 0x0000 
LizardSpriteVRAM: 	equ PixelFontVRAM+PixelFontSizeB 
BearSpriteVRAM: 	equ LizardSpriteVRAM+LizardSpriteSizeB 

; Include art assets 
        include 'assets\fonts\pixelfont.asm' 
        include 'assets\sprites\lizard.asm' 
        include 'assets\sprites\bear.asm' 

; Palettes 
	include 'assets\palettes\palettes.asm'
	include 'assets\palettes\lizard.asm'
	include 'assets\palettes\bear.asm'
