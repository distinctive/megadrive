pad_data_a          equ 0x00A10003 
pad_data_b          equ 0x00A10005 

pad_button_up       equ 0x0
pad_button_down     equ 0x1
pad_button_left     equ 0x2
pad_button_right    equ 0x3
pad_button_a        equ 0xC
pad_button_b        equ 0x4
pad_button_c        equ 0x5
pad_button_start    equ 0xD
pad_button_dpad     equ 0x0

ReadPad1: 
; d0 - Return result 

    ; Returns a word but reads one byte at a time, so we have to read, 
    ; shift the byte and read again to form the word. 

    move.b  pad_data_a, d0      ; Read the upper byte 
    rol.w   #0x8, d0            ; Move to upper byte 
    move.b  #0x40, pad_data_a   ; Write bit 7 to data port to read again 
    move.b  pad_data_a, d0      ; Read lower from data port and copy to lower byte 
    move.b  #0x00, pad_data_a   ; Put data port back to normal 

    rts 
