	; Align 8 bytes
	nop 0,8

LizardSprite: 
* --------------------------
	dc.l	$00001101
	dc.l	$00003333
	dc.l	$0003BBBB
	dc.l	$003BB2BB
	dc.l	$03BBB129
	dc.l	$3BBBB999
	dc.l	$3BB99331
	dc.l	$03336BBB

* --------------------------
	dc.l	$000006FB
	dc.l	$00003ADE
	dc.l	$00003FDD
	dc.l	$0003BFDD
	dc.l	$0003BFD3
	dc.l	$0003B5FD
	dc.l	$0003B55F
	dc.l	$03BB3005

* --------------------------
	dc.l	$035B3000
	dc.l	$00033005
	dc.l	$0000003B
	dc.l	$00000059
	dc.l	$00000059
	dc.l	$00003339
	dc.l	$0007BBB9
	dc.l	$00033333

* --------------------------
	dc.l	$01000000
	dc.l	$11000000
	dc.l	$71100000
	dc.l	$98000000
	dc.l	$91100000
	dc.l	$50000000
	dc.l	$10000000
	dc.l	$33000000

* --------------------------
	dc.l	$93300000
	dc.l	$B9330000
	dc.l	$C3B93000
	dc.l	$DD3B9300
	dc.l	$63BB9300
	dc.l	$3BB33307
	dc.l	$D333B946
	dc.l	$FDDDBBBB

* --------------------------
	dc.l	$5FDDDBBB
	dc.l	$9FFFDDCB
	dc.l	$96333338
	dc.l	$93000003
	dc.l	$30000000
	dc.l	$30000000
	dc.l	$30000000
	dc.l	$30000000

* --------------------------
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000030
	dc.l	$00000393
	dc.l	$00000493

* --------------------------
	dc.l	$00004B93
	dc.l	$0004B930
	dc.l	$004BB930
	dc.l	$04BB9300
	dc.l	$4BB93000
	dc.l	$BB930000
	dc.l	$B9300000
	dc.l	$B3000000

* --------------------------
	dc.l	$96000000
	dc.l	$B3000000
	dc.l	$B9400000
	dc.l	$B9300000
	dc.l	$39300000
	dc.l	$39933000
	dc.l	$39999300
	dc.l	$33333300

LizardSprite2: 
* --------------------------
	dc.l	$00000110
	dc.l	$00000333
	dc.l	$00003BBB
	dc.l	$0003BB2B
	dc.l	$003BBB12
	dc.l	$03BBBB99
	dc.l	$03BB9933
	dc.l	$003336BB

* --------------------------
	dc.l	$0000006B
	dc.l	$0000003E
	dc.l	$0000003D
	dc.l	$000003BD
	dc.l	$000003B3
	dc.l	$000003BD
	dc.l	$000003B5
	dc.l	$0003BB30

* --------------------------
	dc.l	$00035B30
	dc.l	$00000330
	dc.l	$00000003
	dc.l	$00000005
	dc.l	$00000005
	dc.l	$00000033
	dc.l	$000003BB
	dc.l	$00000333

* --------------------------
	dc.l	$10100000
	dc.l	$31100000
	dc.l	$B7110000
	dc.l	$B9800000
	dc.l	$99110000
	dc.l	$95000000
	dc.l	$11000000
	dc.l	$33000000

* --------------------------
	dc.l	$93300000
	dc.l	$B9330000
	dc.l	$C3B33000
	dc.l	$DD3B3000
	dc.l	$63BB3000
	dc.l	$3BB33007
	dc.l	$3333B946
	dc.l	$5DDDBBBB

* --------------------------
	dc.l	$05FDBB96
	dc.l	$59FFCBB3
	dc.l	$B96338B9
	dc.l	$993003B9
	dc.l	$93000039
	dc.l	$93000039
	dc.l	$99300039
	dc.l	$33300033

* --------------------------
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000030
	dc.l	$00000330
	dc.l	$00003930

* --------------------------
	dc.l	$00004930
	dc.l	$0004B930
	dc.l	$004BB930
	dc.l	$04BB9300
	dc.l	$4BB93000
	dc.l	$BB930000
	dc.l	$B9300000
	dc.l	$B3000000

* --------------------------
	dc.l	$30000000
	dc.l	$00000000
	dc.l	$40000000
	dc.l	$30000000
	dc.l	$30000000
	dc.l	$93300000
	dc.l	$99930000
	dc.l	$33330000

LizardSprite3: 
* --------------------------
	dc.l	$00000011
	dc.l	$00000033
	dc.l	$000003BB
	dc.l	$00003BB2
	dc.l	$0003BBB1
	dc.l	$003BBBB9
	dc.l	$003BB993
	dc.l	$00033363

* --------------------------
	dc.l	$00000005
	dc.l	$00000005
	dc.l	$00000005
	dc.l	$00000005
	dc.l	$00000055
	dc.l	$000005B5
	dc.l	$00000555
	dc.l	$00000000

* --------------------------
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000005
	dc.l	$00000005
	dc.l	$00000005

* --------------------------
	dc.l	$01010000
	dc.l	$33110000
	dc.l	$BB711000
	dc.l	$BB980000
	dc.l	$29911000
	dc.l	$99500000
	dc.l	$31100000
	dc.l	$33500000

* --------------------------
	dc.l	$FFB50000
	dc.l	$FDB95000
	dc.l	$FD595000
	dc.l	$FD595000
	dc.l	$F5595500
	dc.l	$55B95507
	dc.l	$5BB95546
	dc.l	$55955BBB

* --------------------------
	dc.l	$0555DB99
	dc.l	$09FFD944
	dc.l	$058BB900
	dc.l	$053BB300
	dc.l	$03B3B300
	dc.l	$533BB930
	dc.l	$93BB9930
	dc.l	$93333330

* --------------------------
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00004000
	dc.l	$00004000

* --------------------------
	dc.l	$00049400
	dc.l	$004B9400
	dc.l	$004B9400
	dc.l	$04B93000
	dc.l	$4BB93000
	dc.l	$BB930000
	dc.l	$B9300000
	dc.l	$93000000

* --------------------------
	dc.l	$30000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000

LizardSprite4: 
* --------------------------
	dc.l	$00000001
	dc.l	$00000003
	dc.l	$0000003B
	dc.l	$000003BB
	dc.l	$00003BBB
	dc.l	$0003BBBB
	dc.l	$0003BB99
	dc.l	$00003336

* --------------------------
	dc.l	$00000003
	dc.l	$00000003
	dc.l	$00000003
	dc.l	$00000003
	dc.l	$00000333
	dc.l	$00003BBB
	dc.l	$00003B33
	dc.l	$00003B30

* --------------------------
	dc.l	$00000330
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000033
	dc.l	$000003BB
	dc.l	$00000333

* --------------------------
	dc.l	$10101000
	dc.l	$33311000
	dc.l	$BBB71100
	dc.l	$2BB98000
	dc.l	$12991100
	dc.l	$99950000
	dc.l	$33110000
	dc.l	$33B30000

* --------------------------
	dc.l	$BB333000
	dc.l	$DBBB3000
	dc.l	$F3BBB300
	dc.l	$F3BBB300
	dc.l	$33B53300
	dc.l	$BBB5B304
	dc.l	$333DBB4B
	dc.l	$003DBBBB

* --------------------------
	dc.l	$003FDBBB
	dc.l	$03FBBB43
	dc.l	$03BBB333
	dc.l	$03BB3393
	dc.l	$3BB33993
	dc.l	$BB333393
	dc.l	$BB300399
	dc.l	$33303333

* --------------------------
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00030000
	dc.l	$00030000
	dc.l	$003B3000

* --------------------------
	dc.l	$03BB3000
	dc.l	$03B30000
	dc.l	$03B30000
	dc.l	$34B30000
	dc.l	$4BB30000
	dc.l	$BB330000
	dc.l	$B9300000
	dc.l	$B3000000

* --------------------------
	dc.l	$30000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$00000000
	dc.l	$30000000
	dc.l	$33000000

* --------------------------

LizardSpriteEnd 
LizardSpriteSizeB: 		equ (LizardSpriteEnd-LizardSprite) 			; Size in bytes 
LizardSpriteSizeT: 		equ (LizardSpriteSizeB/32)					; Size in tiles 
LizardSpriteSizeF:		equ (LizardSpriteSizeT/9)					; Size in frames 
LizardSpriteOneFrameT: 	equ (LizardSpriteSizeT/LizardSpriteSizeF)	; Size of one frame 
LizardSpriteOneFrameB: 	equ (LizardSpriteSizeB/LizardSpriteSizeF)	; Size of one frame 
LizardSpriteTileID: 	equ (LizardSpriteVRAM/32) 					; ID of first tile 
LizardSpriteDimensions: equ (%1010) 								; Dimensions, 3x3 

; Animation frame data 
LizardSpriteAnimData: 
	dc.b 	0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1
LizardSpriteAnimDataEnd 
LizardSpriteNumFrames: 	equ (LizardSpriteAnimDataEnd-LizardSpriteAnimData)
