; Position: 
; SSSD DDDD DDDD YYYY YYYY YYXX XXXX XXXX
; (S)peed, (D)irection, (Y) pos, (X) pos 
;player1_position    equ 0x00000000

; Animation data: 
; NNNN NNNN SSSS SSSS FFFF FFFF
; Anim (N)umber, (S)peed, (F)rame index 
;player1_animation   equ 0x00000000

player1_pos_x       equ 0x0000
player1_pos_y       equ 0x0000
player1_dir         equ 0x0000
player1_speed       equ 0x00
player1_anim_frame  equ 0x00
player1_anim_speed  equ 0x00
player1_anim_num    equ 0x00
