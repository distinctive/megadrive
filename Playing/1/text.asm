        include 'charmap.asm' 

	; Align 8 bytes
	nop 0,8

DrawTextPlaneA: 
; a0 (l) - String address 
; d0 - First file ID of font 
; d1 (bb) - XY coord (in tiles) 
; d2 (b) - Palette 

        clr.l   d3              ; Clear d3
        move.b  d1, d3          ; Move the Y coord to d3 
        mulu.w  #0x0040, d3     ; Multiply Y coord at d3 by 64 to get line coord 
        ror.l   #0x8, d1        ; Bit-shift, move the X coord across 
        add.b   d1, d3          ; Add the X coordinate to the offset 
        mulu.w  #0x2, d3        ; Multiply, unsigned, Convert to words 
        swap    d3              ; Move to the upper word 
        add.l   #vdp_write_plane_a, d3 ; Plane A write + address 
        move.l  d3, vdp_control ; Send to VDP control port 

        clr.l   d3              ; Clear d3
        move.b  d2, d3          ; Move the pallete ID from d2 to d3 
        rol.l   #0x8, d3        ; Shift palette ID to bits 15 and 15 of d3 
        rol.l   #0x5, d3        ; Finish the shift, can only roll 8 bits in one instruction 

        lea     ASCIIMap, a1 

        @CharCopy: 
        move.b  (a0)+, d2       ; Move to next char 
        cmp.b   #0x0, d2        ; Test if zero terminator char 
        beq.b   @End            ; If zero, branch to end 

        sub.b   #ASCIIStart, d2 ; 
        move.b  (a1,d2.w), d3   ; Move tile ID from table to lower byte d3 
        add.w   d0, d3          ; Offset the file ID to `d3` as first tile ID in font 
        move.w  d3, vdp_data    ; Move palette and pattern IDs to VDP data port 
        jmp     @CharCopy       ; Next character 

        @End: 
        rts 
