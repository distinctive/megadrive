        include 'init.asm' 
        include 'globals.asm' 
        include 'text.asm' 
        include 'graphics.asm' 
        include 'gamepad.asm' 
        
LLizString1: 
        dc.b    "LLIZ DURP!",0

LLizString2: 
        dc.b    "FOR ALL OF YOUR SCALEY NEEDS",0

SpriteDesc: 
        dc.w    0x0000 
        dc.b    %00001010
        dc.b    0x01                    ; Next sprite index 
        dc.b    %01000000
        dc.b    LizardSpriteTileID 
        dc.w    0x0000 

        dc.w    0x0000 
        dc.b    %00001111
        dc.b    0x00 
        dc.b    %01100000
        dc.b    BearSpriteTileID 
        dc.w    0x0000 

lizard_anim_frame       equ (vblank_counter+SizeByte) 

        include 'assetmap.asm' 
        include 'player.asm' 

__main: 

        move.w  #0x8F02, vdp_control    ; Set auto-increment to 2 bytes 

; ****************************************
; Move palettes to CRAM 
; ****************************************

        lea     Palettes, a0            ; Load address of Palettes into a0 
        move.w  #0x0002, d0             ; Set loop count (8) into d0 
        move.w  #0x0000, d1             ; First index 
        jsr     LoadPalette 

        lea     LizardPalette, a0 
        move.w  #0x0001, d0 
        move.w  #0x0002, d1             ; 3rd slot (index 2)
        jsr     LoadPalette 

        lea     BearPalette, a0 
        move.w  #0x0001, d0 
        move.w  #0x0003, d1             ; 4th slot (index 3)
        jsr     LoadPalette 

        move.w  #0x870F, vdp_control    ; Set BG colour, palette 0, colour 8 

; ****************************************
; Load font 
; ****************************************
        lea     PixelFont, a0           ; Load effective address 
        move.l  #PixelFontVRAM, d0      ; VRAM address to `d0` 
        move.l  #PixelFontSizeT, d1     ; Numnber of tiles to `d1` 
        jsr     LoadTiles               ; Invoke `LoadTiles` 

; ****************************************
; Load sprite 
; ****************************************
        lea     LizardSprite, a0        ; Load effective address 
        move.l  #LizardSpriteVRAM, d0   ; VRAM address to `d0` 
        move.l  #LizardSpriteOneFrameT, d1  ; Numnber of tiles to `d1` 
        jsr     LoadTiles               ; Invoke `LoadTiles` 

        lea     BearSprite, a0          ; Load effective address 
        move.l  #BearSpriteVRAM, d0     ; VRAM address to `d0` 
        move.l  #BearSpriteSizeT, d1    ; Numnber of tiles to `d1` 
        jsr     LoadTiles               ; Invoke `LoadTiles` 

        ; Create the sprites 
        lea     SpriteDesc, a0 
        move.w  #0x2, d0 
        jsr     LoadSpriteTables 

; ****************************************
; Draw text 
; ****************************************
        lea     LLizString1, a0         ; String address 
        move.l  #PixelFontTileID, d0    ; First tile ID 
        move.w  #0x0501, d1             ; XY (5, 1) 
        move.l  #0x0, d2                ; Palette 0 
        jsr     DrawTextPlaneA          ; Call the draw text subroutine 

        lea     LLizString2, a0         ; String address 
        move.l  #PixelFontTileID, d0    ; First tile ID 
        move.w  #0x0502, d1             ; XY (5, 1) 
        move.l  #0x1, d2                ; Palette 0 
        jsr     DrawTextPlaneA          ; Call the draw text subroutine 

        ; Move the sprites to initial positions 
        ; Lizard 
        move.w  #0x0, d0 
        move.w  #0xB0, d1 
        jsr     SetSpritePosX 
        move.w  #0xD2, d1 
        jsr     SetSpritePosY 

        ; Bear 
        move.w  #0x1, d0 
        move.w  #0xFA, d1 
        jsr     SetSpritePosX 
        move.w  #0xD2, d1 
        jsr     SetSpritePosY 

        ; Text into VRAM 
        ; move.l #0x40200000, vdp_control ; VDP write to 0x0020 
        ; lea Characters, a0 ; Load address of Characters into a0 
        ; move.l #0x37, d0 ; Load 32*7 bytes of data 

        ; @Loop2: 
        ; move.l (a0)+, vdp_data ; Move data to the VDP data port 
        ; dbra d0, @Loop2 

        ; Write HELLO WORLD 
        ; move.l #vdp_write_plane_a, vdp_control ; Tell VDP to write to plane `a` 
        ; move.w #0x0001, vdp_data 
        ; move.w #0x0002, vdp_data 
        ; move.w #0x0003, vdp_data 
        ; move.w #0x0003, vdp_data 
        ; move.w #0x0004, vdp_data      
        ; move.w #0x0000, vdp_data 
        ; move.w #0x0005, vdp_data 
        ; move.w #0x0004, vdp_data 
        ; move.w #0x0006, vdp_data 
        ; move.w #0x0003, vdp_data 
        ; move.w #0x0007, vdp_data 

        move.l  #0xB0, d4               ; Store X pos in d4 
        move.l  #0xD2, d5               ; Store Y pos in d5 
        move.l  #0x0, d6                ; Store extended attributes in d6 

GameLoop: 

        ; Read lizard data 
        move    #0x0, d0                ; Lizard sprite ID 

        move    #sprite_attr_x, d1      ; Read X pos 
        jsr     ReadSpriteAttr 
        move    d2, d4                  ; Save to d4 

        move    #sprite_attr_y, d1      ; Read Y pos 
        jsr     ReadSpriteAttr
        move    d2, d5                 ; Save to d5 

        move    #sprite_attr_extended, d1 ; Read extended attributes 
        jsr     ReadSpriteAttr
        move    d2, d6                 ; Save to d6 

        ; Read gamepad input 
        jsr     ReadPad1                ; Read pad 1 to d6 

        ; Calculate movement 
        move.l  #0x1, d7                ; Default sprite speed in d6 

        btst    #pad_button_a, d0       ; Check A button 
        bne     @noAPress 
        move.l  #0x2, d7                ; If A pressed then move 
        @noAPress: 

        btst    #pad_button_b, d0       ; Check A button 
        bne     @noBPress 
        move.l  #0x3, d7                ; If B pressed then double speed  
        @noBPress: 

        btst    #pad_button_c, d0       ; Check A button 
        bne     @noCPress 
        move.l  #0x4, d7                ; If B pressed then double speed  
        @noCPress: 

        btst    #pad_button_right, d0   ; Check right button 
        bne     @noRightPress 
        add.w   d7, d4                  ; Increment sprite X pos by move speed 
        bset    #0xB, d6 
        @noRightPress: 

        btst    #pad_button_left, d0    ; Check left button 
        bne     @noLeftPress 
        sub.w   d7, d4                  ; Decrement sprite X pos by move speed 
        bclr    #0xB, d6 
        @noLeftPress: 

        btst    #pad_button_down, d0    ; Check for down button press 
        bne     @noDownPress 
        add.w   d7, d5                  ; Increment Y pos by move speed 
        @noDownPress: 

        btst    #pad_button_up, d0      ; Check up button 
        bne     @noUpPress 
        sub.w   d7, d5                  ; Decrememnt Y pos by move speed 
        @noUpPress: 

        ; Update sprites only during VBlank 
        jsr     WaitVBlankStart 

                ; Move the lizard 
                move.w  #0x0, d0                ; Lizard sprite ID 
                move.w  d4, d1                  ; X coord 
                jsr     SetSpritePosX           ; Set X 
                move.w  d5, d1                  ; Y coord 
                jsr     SetSpritePosY           ; Set Y 
                move.w  d6, d1                  ; Y coord 
                jsr     SetSpriteExtended       ; Set extended attrs 

                jsr     ReadPad1                ; Read pad 1 to d6 

                btst    #pad_button_right, d0   ; Check right button 
                bne     @noRightPress2 
                jsr     AnimateLizard 
                @noRightPress2: 

                btst    #pad_button_left, d0    ; Check left button 
                bne     @noLeftPress2 
                jsr     AnimateLizard 
                @noLeftPress2: 

                btst    #pad_button_down, d0    ; Check for down button press 
                bne     @noDownPress2 
                jsr     AnimateLizard 
                @noDownPress2: 

                btst    #pad_button_up, d0      ; Check up button 
                bne     @noUpPress2 
                jsr     AnimateLizard 
                @noUpPress2: 

        jsr     WaitVBlankEnd 

        jmp     GameLoop                ; Restart game loop 

AnimateLizard: 

        ; Lizard animation 
        move.l  #LizardSpriteVRAM, d0 
        move.l  #LizardSpriteOneFrameB, d1 
        move.l  #LizardSpriteNumFrames, d2 
        lea     LizardSprite, a0 
        lea     LizardSpriteAnimData, a1 
        lea     lizard_anim_frame, a2 
        jsr     AnimateSpriteFwd 

        rts 

__end 